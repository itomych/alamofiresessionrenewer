//
//  AlamofireSessionRenewer.h
//  AlamofireSessionRenewer
//
//  Created by Yakov Shkolnikov on 25/11/2019.
//  Copyright © 2019 DashDevs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AlamofireSessionRenewer.
FOUNDATION_EXPORT double AlamofireSessionRenewerVersionNumber;

//! Project version string for AlamofireSessionRenewer.
FOUNDATION_EXPORT const unsigned char AlamofireSessionRenewerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AlamofireSessionRenewer/PublicHeader.h>


