//
//  SessionManager+RequestMock.swift
//  ExampleTests
//
//  Created by Yakov Shkolnikov on 25/11/2019.
//  Copyright © 2019 DashDevs LLC. All rights reserved.
//

@testable import AlamofireSessionRenewer
import Alamofire

extension SessionManager {
    func request(with requestInfo: MockURLRequestInfo) -> DataRequest {
        let headers: HTTPHeaders = [
            MockDurationKey: String(requestInfo.duration)
        ]
        return request(requestInfo.url, headers: headers).validate(MockResponseValidator)
    }
}
