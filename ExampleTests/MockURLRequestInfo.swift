//
//  MockURLRequestInfo.swift
//  ExampleTests
//
//  Created by Yakov Shkolnikov on 25/11/2019.
//  Copyright © 2019 DashDevs LLC. All rights reserved.
//

import Foundation

struct MockURLRequestInfo: Codable {
    let url: URL
    let duration: Int
}
