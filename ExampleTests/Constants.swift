//
//  Constants.swift
//  ExampleTests
//
//  Created by Yakov Shkolnikov on 25/11/2019.
//  Copyright © 2019 DashDevs LLC. All rights reserved.
//

let MockAuthorizedCredential = "Authorized"
let MockUnauthorizedCredential = "Unauthorized"
let MockCredentialHeaderField = "Authorization"
let MockDurationKey = "Duration"
let MockAuthenticationFailureCode = 401
let MockAuthenticationSuccessCode = 200
let MockMaxRetryCount: UInt = 1
